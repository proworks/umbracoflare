﻿using UmbracoFlare.Configuration;
using UmbracoFlare.Manager;
using UmbracoFlare.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Publishing;
using Umbraco.Core.Services;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Trees;
using UmbracoFlare.ImageCropperHelpers;
using UmbracoFlare.Models.CropModels;
using Umbraco.Web;
using Umbraco.Core.Logging;
using UmbracoFlare.Helpers;
using Umbraco.Web.Cache;
using Umbraco.Core.Cache;
using UmbracoFlare.ApiControllers;
using Examine;
using Newtonsoft.Json.Linq;

namespace UmbracoFlare.App_Start
{
    public class SetCloudflareHooks : ApplicationEventHandler
    {
        private const string BodyContentComponentTypesField = "umbracoFlareBodyContentComponentTypes";

        public SetCloudflareHooks()
            : base()
        {
            ContentService.Published += PurgeCloudflareCache;
            ContentService.Published += UpdateContentIdToUrlCache;
            FileService.SavedScript += PurgeCloudflareCacheForScripts;
            FileService.SavedStylesheet += PurgeCloudflareCacheForStylesheets;

            MediaService.Saved += PurgeCloudflareCacheForMedia;
            DataTypeService.Saved += RefreshImageCropsCache;
            TreeControllerBase.MenuRendering += AddPurgeCacheForContentMenu;

            ExamineManager.Instance.IndexProviderCollection[Umbraco.Core.Constants.Examine.ExternalIndexer].GatheringNodeData += SetCloudflareHooks_GatheringNodeData;
        }

        private void SetCloudflareHooks_GatheringNodeData(object sender, IndexingNodeDataEventArgs e)
        {
            try
            {
                if (!e.Fields.TryGetValue("bodyContent", out var content) || string.IsNullOrEmpty(content) || content[0] != '[') return;

                var json = JArray.Parse(content);
                var componentTypes = new List<Guid>();

                foreach (JObject element in json)
                {
                    if (Guid.TryParse(element?["icContentTypeGuid"]?.ToString(), out var guid) && !componentTypes.Contains(guid)) componentTypes.Add(guid);
                }

                e.Fields[BodyContentComponentTypesField] = string.Join(" ", componentTypes.Select(x => x.ToString().Replace("-", "")));
            }
            catch
            {
            }
        }

        protected void UpdateContentIdToUrlCache(IPublishingStrategy strategy, PublishEventArgs<IContent> e)
        {
            UmbracoHelper uh = new UmbracoHelper(UmbracoContext.Current);
            
            foreach(IContent c in e.PublishedEntities)
            {
                if(c.HasPublishedVersion)
                {
                    IEnumerable<string> urls = UmbracoFlareDomainManager.Instance.GetUrlsForNode(c.Id, false);

                    if(urls.Contains("#"))
                    {
                        //When a piece of content is first saved, we cannot get the url, if that is the case then we need to just
                        //invalidate the who ContentIdToUrlCache, that way when we request all of the urls agian, it will pick it up.
                        UmbracoUrlWildCardManager.Instance.DeletedContentIdToUrlCache();
                    }
                    else
                    {
                        UmbracoUrlWildCardManager.Instance.UpdateContentIdToUrlCache(c.Id, urls);
                    }

                }   


                //TODO: Does this need to be here?
                //We also need to update the descendants now because their urls changed
                IEnumerable<IContent> descendants = c.Descendants();

                foreach(IContent desc in descendants)
                {
                    IEnumerable<string> descUrls = UmbracoFlareDomainManager.Instance.GetUrlsForNode(desc.Id, false);

                    UmbracoUrlWildCardManager.Instance.UpdateContentIdToUrlCache(c.Id, descUrls);
                }
            }
        }

        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            base.ApplicationStarting(umbracoApplication, applicationContext);
        }


        protected void RefreshImageCropsCache(IDataTypeService sender, SaveEventArgs<IDataTypeDefinition> e)
        {
            //A data type has saved, see if it was a 
            IEnumerable<IDataTypeDefinition> imageCroppers = ImageCropperManager.Instance.GetImageCropperDataTypes(true);
            IEnumerable<IDataTypeDefinition> freshlySavedImageCropper = imageCroppers.Intersect(e.SavedEntities);

            if(imageCroppers.Intersect(e.SavedEntities).Any())
            {
                //There were some freshly saved Image cropper data types so refresh the image crop cache.
                //We can do that by simply getting the crops
                ImageCropperManager.Instance.GetAllCrops(true); //true to bypass the cache & refresh it.
            }
        }

        private void PurgeCloudflareCacheForScripts(IFileService sender, SaveEventArgs<Script> e)
        {
            var files = e.SavedEntities.Select(se => se as File);
            PurgeCloudflareCacheForFiles(files, e);
        }
        private void PurgeCloudflareCacheForStylesheets(IFileService sender, SaveEventArgs<Stylesheet> e)
        {
            var files = e.SavedEntities.Select(se => se as File);
            PurgeCloudflareCacheForFiles(files, e);
        }
        private void PurgeCloudflareCacheForFiles<T>(IEnumerable<File> files, SaveEventArgs<T> e)
        {
            //If we have the cache buster turned off then just return.
            if (!CloudflareConfiguration.Instance.PurgeCacheOn) { return; }

            List<string> urls = new List<string>();
            UmbracoHelper uh = new UmbracoHelper(UmbracoContext.Current);
            //GetUmbracoDomains
            IEnumerable<string> domains = UmbracoFlareDomainManager.Instance.AllowedDomains;

            foreach (var file in files)
            {
                if (file.IsNewEntity())
                {
                    //If its new we don't want to purge the cache as this causes slow upload times.
                    continue;
                }

                urls.Add(file.VirtualPath);
            }


            List<StatusWithMessage> results = CloudflareManager.Instance.PurgePages(UrlHelper.MakeFullUrlWithDomain(urls, domains, true));

            if (results.Any() && results.Where(x => !x.Success).Any())
            {
                e.Messages.Add(new EventMessage("Cloudflare Caching", "We could not purge the Cloudflare cache. \n \n" + CloudflareManager.PrintResultsSummary(results), EventMessageType.Warning));
            }
            else if (results.Any())
            {
                e.Messages.Add(new EventMessage("Cloudflare Caching", "Successfully purged the cloudflare cache.", EventMessageType.Success));
            }
        }


        protected void PurgeCloudflareCacheForMedia(IMediaService sender, SaveEventArgs<IMedia> e)
        {
            //If we have the cache buster turned off then just return.
            if (!CloudflareConfiguration.Instance.PurgeCacheOn) { return; }

            List<Crop> imageCropSizes = ImageCropperManager.Instance.GetAllCrops();
            List<string> urls = new List<string>();

            UmbracoHelper uh = new UmbracoHelper(UmbracoContext.Current);

            //GetUmbracoDomains
            IEnumerable<string> domains = UmbracoFlareDomainManager.Instance.FullAllowedDomains;
           
            //delete the cloudflare cache for the saved entities.
            foreach (IMedia media in e.SavedEntities)
            {
                if(media.IsNewEntity())
                {
                    //If its new we don't want to purge the cache as this causes slow upload times.
                    continue;
                }

                try
                {
                    //Check to see if the page has cache purging on publish disabled.
                    if (media.GetValue<bool>("cloudflareDisabledOnPublish"))
                    {
                        //it was disabled so just continue;
                        continue;
                    }
                }
                catch (Exception ex)
                {
                    //continue;
                } 

                IPublishedContent publishedMedia = uh.TypedMedia(media.Id);

                if (publishedMedia == null)
                {
                    e.Messages.Add(new EventMessage("Cloudflare Caching", "We could not find the IPublishedContent version of the media: "+media.Id+" you are trying to save.", EventMessageType.Error));
                    continue;
                }
                foreach(Crop crop in imageCropSizes)
                {
                    urls.Add(publishedMedia.GetCropUrl(crop.alias));    

                }
                urls.Add(publishedMedia.Url);
            }


            List<StatusWithMessage> results = CloudflareManager.Instance.PurgePages(UrlHelper.MakeFullUrlWithDomain(urls, domains,true));

            if (results.Any() && results.Where(x => !x.Success).Any())
            {
                e.Messages.Add(new EventMessage("Cloudflare Caching", "We could not purge the Cloudflare cache. \n \n" + CloudflareManager.PrintResultsSummary(results), EventMessageType.Warning));
            }
            else if(results.Any())
            {
                e.Messages.Add(new EventMessage("Cloudflare Caching", "Successfully purged the cloudflare cache.", EventMessageType.Success));
            }
        }

        

        protected void PurgeCloudflareCache(IPublishingStrategy strategy, PublishEventArgs<IContent> e)
        {
            //If we have the cache buster turned off then just return.
            if (!CloudflareConfiguration.Instance.PurgeCacheOn) { return; }

            var urls = new List<string>();

            // This should be saved and not reloaded
            var alwaysPurgeNodes = CloudflareConfiguration.Instance.AlwaysPurgeNodes;
            var ancestorPurgeTypes = CloudflareConfiguration.Instance.AncestorPurgeTypes;
            var typeRelatedComponents = CloudflareConfiguration.Instance.TypeRelatedComponents;
            var umbracoContext = UmbracoContext.Current;

            foreach (var key in alwaysPurgeNodes)
            {
                var node = umbracoContext.ContentCache.GetById(key);
                if (node != null) urls.AddRange(UmbracoFlareDomainManager.Instance.GetUrlsForNode(node, false));
            }

            //Else we can continue to delete the cache for the saved entities.
            foreach (IContent content in e.PublishedEntities)
            {
                try
                {
                    //Check to see if the page has cache purging on publish disabled.
                    if (content.GetValue<bool>("cloudflareDisabledOnPublish"))
                    {
                        //it was disabled so just continue;
                        continue;
                    }
                }
                catch (Exception ex)
                {
                    //continue;
                }

                var pubContent = umbracoContext.ContentCache.GetById(content.Id);

                if (pubContent != null)
                {
                    foreach (var anc in pubContent.AncestorsOrSelf())
                    {
                        var alias = anc.ContentType.Alias;

                        if (ancestorPurgeTypes.TryGetValue(alias, out var purgeChildren)) urls.AddRange(UmbracoFlareDomainManager.Instance.GetUrlsForNode(anc, purgeChildren));
                    }

                    if (typeRelatedComponents.TryGetValue(pubContent.ContentType.Alias, out var componentAliases))
                    {
                        var nodeIds = FindNodeIdsUsingComponents(componentAliases);
                        foreach (var nodeId in nodeIds) urls.AddRange(UmbracoFlareDomainManager.Instance.GetUrlsForNode(nodeId));
                    }
                }

                urls.AddRange(UmbracoFlareDomainManager.Instance.GetUrlsForNode(content.Id, false));
            }

            List<StatusWithMessage> results = CloudflareManager.Instance.PurgePages(urls);

            if (results.Any() && results.Where(x => !x.Success).Any())
            {
                e.Messages.Add(new EventMessage("Cloudflare Caching", "We could not purge the Cloudflare cache. \n \n" + CloudflareManager.PrintResultsSummary(results), EventMessageType.Warning));
            }
            else if (results.Any())
            {
                e.Messages.Add(new EventMessage("Cloudflare Caching", "Successfully purged the cloudflare cache.", EventMessageType.Success));
            }
        }

        private IEnumerable<int> FindNodeIdsUsingComponents(IEnumerable<Guid> componentAliases)
        {
            var searcher = ExamineManager.Instance.SearchProviderCollection[Umbraco.Core.Constants.Examine.ExternalSearcher];
            var criteria = searcher.CreateSearchCriteria();

            criteria.GroupedOr(new[] { BodyContentComponentTypesField }, componentAliases.Select(x => x.ToString().Replace("-", "")).ToArray());

            var results = searcher.Search(criteria);
            return results.Select(x => x.Id);
        }

        private void AddPurgeCacheForContentMenu(TreeControllerBase sender, MenuRenderingEventArgs args)
        {
            if(sender.TreeAlias != "content")
            {
                //We aren't dealing with the content menu
                return;
            }

            MenuItem menuItem = new MenuItem("purgeCache", "Purge Cloudflare Cache");

            menuItem.Icon = "umbracoflare-tiny";

            menuItem.LaunchDialogView("/App_Plugins/UmbracoFlare/backoffice/treeViews/PurgeCacheDialog.html", "Purge Cloudflare Cache");

            args.Menu.Items.Insert(args.Menu.Items.Count - 1, menuItem);
        }

    }
}
