﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Xml.Linq;
using umbraco.presentation;
using Umbraco.Core;

namespace UmbracoFlare.Configuration
{
    public class CloudflareConfiguration 
    {
        public static string CONFIG_PATH;
        private static CloudflareConfiguration _instance = null;
        private XDocument _doc = null;

        private CloudflareConfiguration()
        {
            try
            {
                CONFIG_PATH = HttpContext.Current.Server.MapPath("~/Config/cloudflare.config");
                this._doc = XDocument.Load(CONFIG_PATH);
            }
            catch(Exception e)
            {

            }
        }

        public static CloudflareConfiguration Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new CloudflareConfiguration();
                }

                return _instance;
            }
        }
        

        public string ApiKey
        {
            get
            {
                return this._doc==null ? String.Empty: this._doc.Root.Element("apiKey").Value;
            }
            set
            {
                this._doc.Root.Element("apiKey").SetValue(value);
                this._doc.Save(CONFIG_PATH);
            }
        }
        
        
        public string AccountEmail
        {
            get
            {
                return this._doc == null ? String.Empty : this._doc.Root.Element("accountEmail").Value;
            }
            set
            {
                this._doc.Root.Element("accountEmail").SetValue(value);
                this._doc.Save(CONFIG_PATH);
            }
        }

        
        public bool PurgeCacheOn 
        {
            get
            {
                bool purgeCacheOn = false;
                if(this._doc!=null)
                {
                    bool.TryParse(this._doc.Root.Element("purgeCacheOn").Value, out purgeCacheOn);
                }
                return purgeCacheOn;
            }

            set
            {
                this._doc.Root.Element("purgeCacheOn").SetValue(value.ToString());
                this._doc.Save(CONFIG_PATH);
            }
        }

        public IReadOnlyDictionary<string, bool> AncestorPurgeTypes =>
            _doc.Root.Element("ancestorPurgeTypes").Elements("documentType")
            .Select(x => new KeyValuePair<string, bool>(x.Attribute("alias")?.Value, bool.TryParse(x.Attribute("purgeAllChildren")?.Value, out var b) && b))
            .Where(x => !string.IsNullOrWhiteSpace(x.Key))
            .ToDictionary(x => x.Key, x => x.Value);

        public IEnumerable<Guid> AlwaysPurgeNodes =>
            _doc.Root.Element("alwaysPurgeNodes").Elements("alwaysPurge")
            .Select(x => Guid.TryParse(x.Attribute("key")?.Value, out var guid) ? guid : Guid.Empty)
            .Where(x => !Guid.Empty.Equals(x));

        public IReadOnlyDictionary<string, IEnumerable<Guid>> TypeRelatedComponents =>
            _doc.Root.Element("typeRelatedComponents").Elements("monitoredDocumentType")
            .Select(x => new KeyValuePair<string, IEnumerable<Guid>>(
                x.Attribute("alias")?.Value,
                x.Elements("componentDocumentType").Select(y => Guid.TryParse(y.Attribute("guid")?.Value, out var g) ? g : Guid.Empty).Where(y => !Guid.Empty.Equals(y)).ToList()
                )
            )
            .Where(x => !string.IsNullOrWhiteSpace(x.Key) && x.Value.Any())
            .ToDictionary(x => x.Key, x => x.Value);

        /*
        public List<string> AdditionalZoneUrls
        {
            get
            {
                string additionalZoneUrlsCommaSep = this._doc.Root.Element("additionalZoneUrls").Value;

                if(!String.IsNullOrEmpty(additionalZoneUrlsCommaSep))
                {
                    return additionalZoneUrlsCommaSep.Split(',').ToList();
                }
                else
                {
                    return new List<string>();
                }
            }
        }*/
    }
}
