﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using UmbracoFlare.Helpers;
using UmbracoFlare.Models;
using Umbraco.Core.Logging;

namespace UmbracoFlare.Manager
{
    public class UmbracoFlareDomainManager
    {
        private static UmbracoFlareDomainManager _instance = null;

        private CloudflareManager _cloudflareManager;

        private IEnumerable<Zone> _allowedZones;
        private IEnumerable<string> _allowedDomains;
        private IEnumerable<string> _fullAllowedDomains;

        private UmbracoFlareDomainManager()
        {
            _cloudflareManager = CloudflareManager.Instance;
        }

        public static UmbracoFlareDomainManager Instance { get; } = new UmbracoFlareDomainManager();

        public IEnumerable<Zone> AllowedZones {
            get
            {
                if(_allowedZones == null) GetAllowedZonesAndDomains();

                return _allowedZones;
            }
        }

        public IEnumerable<string> AllowedDomains
        {
            get
            {
                if (_allowedDomains == null || !_allowedDomains.Any()) GetAllowedZonesAndDomains();

                return _allowedDomains;
            }
        }

        public IEnumerable<string> FullAllowedDomains
        {
            get
            {
                if (_fullAllowedDomains == null || !_fullAllowedDomains.Any()) GetAllowedZonesAndDomains();

                return _fullAllowedDomains;
            }
        }


        /// <summary>
        /// Gets the domains from each cloudflare zone.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetDomainsFromCloudflareZones()
        {
            return this.AllowedZones.Select(x => x.Name);
        }

        /// <summary>
        /// This will take a list of domains and make sure that they are either equal to or a subdomain of the allowed domains in cloudflare.
        /// </summary>
        /// <param name="domains">The domains to filter.</param>
        /// <returns>The filtered list of domains.</returns>
        public IEnumerable<string> FilterToAllowedDomains(IEnumerable<string> domains)
        {
            List<string> filteredDomains = new List<string>();

            foreach(string allowedDomain in this.AllowedDomains)
            {
                foreach(string posDomain in domains)
                {
                    //Is the possible domain an allowed domain? 
                    if(posDomain.Contains(allowedDomain))
                    {
                        if(!filteredDomains.Contains(posDomain))
                        {
                            filteredDomains.Add(posDomain);
                        }
                    }
                }
            }
            return filteredDomains;
        }


        public List<string> GetUrlsForNode(int contentId, bool includeDescendants = false)
        {
            if (includeDescendants)
            {
                var content = UmbracoContext.Current.ContentCache.GetById(contentId);

                if (content == null)
                {
                    return new List<string>();
                }

                return GetUrlsForNode(content, includeDescendants);
            }

            var urls = new List<string>();
            var url = UmbracoContext.Current.RoutingContext.UrlProvider.GetUrl(contentId, true);

            urls.Add(url);
            urls.AddRange(UmbracoContext.Current.RoutingContext.UrlProvider.GetOtherUrls(contentId));

            return urls;
        }

        public List<string> GetUrlsForNode(Guid contentKey, bool includeDescendants = false)
        {
            var content = UmbracoContext.Current.ContentCache.GetById(contentKey);

            if (content == null)
            {
                return new List<string>();
            }

            return GetUrlsForNode(content, includeDescendants);
        }


        /// <summary>
        /// We want to get the domains associated with the content node. This includes getting the allowed domains on the node as well as
        /// searching up the tree to get the parents nodes since they are inherited.
        /// </summary>
        /// <param name="content"></param>
        /// <param name="includeChildren"></param>
        /// <returns></returns>
        public List<string> GetUrlsForNode(IPublishedContent content, bool includeDescendants = false)
        {
            var urls = new List<string>(GetUrlsForNode(content.Id));

            if(includeDescendants)
            {
                foreach(var desc in content.Descendants())
                {
                    urls.Add(UmbracoContext.Current.RoutingContext.UrlProvider.GetUrl(desc.Id, true));
                    urls.AddRange(UmbracoContext.Current.RoutingContext.UrlProvider.GetOtherUrls(desc.Id));
                }
            }

            return urls;
        }

        

        private List<string> RecursivelyGetParentsDomains(List<string> domains, IContent content)
        {
            //Termination case
            if(content == null)
            {
                return domains;
            }

            domains.AddRange(ApplicationContext.Current.Services.DomainService.GetAssignedDomains(content.Id, false).Select(x => x.DomainName));

            domains = RecursivelyGetParentsDomains(domains, content.Parent());

            return domains;
        }

        private List<string> GetDescendantsDomains(List<string> domains, IEnumerable<IContent> descendants)
        {
            if(descendants == null || !descendants.Any())
            {
                return domains;
            }

            foreach(IContent descendant in descendants)
            {
                domains.AddRange(ApplicationContext.Current.Services.DomainService.GetAssignedDomains(descendant.Id, false).Select(x => x.DomainName));
            }

            return domains;
        }

        /// <summary>
        /// This method will grab the list of zones from cloudflare. It will then check to make sure that the zone has some corresponding hostnames in umbraco.
        /// If it does, we will save the domain names & the zones.
        /// </summary>
        /// <returns>A key value pair where the Key is the zones, and the value is the domains</returns>
        private void GetAllowedZonesAndDomains()
        {
            var allowedZones = new List<Zone>();
            var allowedDomains = new List<string>();
            var fullAllowedDomains = new List<string>();

            //Get the list of domains from cloudflare.
            var allZones = _cloudflareManager.ListZones();
            var domainsInUmbraco = ApplicationContext.Current.Services.DomainService.GetAll(false).Select(x => new { Domain = x.DomainName, Host = new UriBuilder(x.DomainName).Uri.DnsSafeHost });

            foreach(var zone in allZones)
            {
                foreach(var pair in domainsInUmbraco)
                {
                    if (pair.Host.Contains(zone.Name)) //if the domain url contains the zone url, then we know its the domain or a sub domain.
                    {
                        if(!allowedZones.Contains(zone))
                        {
                            LogHelper.Info(this.GetType(), "Adding zone " + zone.Name);
                            //The allowed zones doesn't contain this zone yet, add it.
                            allowedZones.Add(zone);
                        }
                        if (!allowedDomains.Contains(pair.Host))
                        {
                            LogHelper.Info(this.GetType(), "Adding domain " + pair.Host);
                            //The allowed domains doens't contain this domain yet, add it.
                            allowedDomains.Add(pair.Host);
                        }
                        if (!fullAllowedDomains.Contains(pair.Domain))
                        {
                            LogHelper.Info(this.GetType(), "Adding full domain " + pair.Domain);
                            //The full allowed domains doens't contain this domain yet, add it.
                            fullAllowedDomains.Add(pair.Domain);
                        }
                    }
                }
            }

            _allowedZones = allowedZones;
            _allowedDomains = allowedDomains;
            _fullAllowedDomains = fullAllowedDomains;
        }
    }
}
